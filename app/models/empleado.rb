class Empleado < ActiveRecord::Base
	belongs_to :cargo
	has_many :recepcions
	#validates :
	validates :Nu_Dni, :No_Empleado, :Tx_Direccion, :Co_Departamento, :Co_Provincia, :Co_Distrito, :Tx_Email, :Fe_Nacimiento, :Tx_telefono, :Fl_Sexo, :Fl_EstadoCivil, :Fe_Contrato, :Fe_Culminacion, :Sa_Salario, presence: true
	validates :Nu_Dni, numericality: { only_integer: true }, uniqueness: true, length: { maximum: 8 }, length: { minimum: 8 }
end
	#validates :equipo1, :equipo2, :arbitro, :estadio, presence: true
	#validates :equipo1, :equipo2, numericality: { only_integer: true }
	#validates :arbitro, :estadio, length: { minimum: 4 }

	#validates :nombre, presence: true, length: { in: 2..20 }, uniqueness: true
	#validates :ciudad, presence: true, length: { maximum: 100 }