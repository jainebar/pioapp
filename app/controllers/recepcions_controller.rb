class RecepcionsController < ApplicationController
  before_action :set_recepcion, only: [:destroy]  
  def index
    @mesas = Mesa.all
  	@recepcions = Recepcion.order("fe_recepcion, hora_recepcion asc")
  end 

  def new
  	@mesas = Mesa.all
    @recepcion = Recepcion.new
  end

  def create
    @est_mesa = Mesa.find(params[:recepcion][:mesa_id])
      @cliente = Cliente.where(:Nu_Dni => params[:Nu_Dni])
      if @cliente.count < 1
        @new_cliente = Cliente.new(:Nu_Dni => params[:Nu_Dni], :No_Cliente => params[:No_Cliente], :Ape_Paterno => params[:Ape_Paterno], :Ape_Mateno => params[:Ape_Mateno])
        @new_cliente.save
        @cliente = Cliente.where(:Nu_Dni => params[:Nu_Dni])
      end 

      @recepcion = Recepcion.new(recepcion_params)

      respond_to do |format|
        if @recepcion.save
          @est_mesa.free = '1'
          @est_mesa.save
          @recepcion.cliente_id = @cliente.first.id
          @recepcion.save
          format.html { redirect_to recepcions_path }
        else
          format.html { render :new }
          format.json { render json: @recepcion.errors, status: :unprocessable_entity }
        end
      end
  end

  def destroy
    @recepcion.destroy
    respond_to do |format|
      @upd_mesa.free = '0'
      @upd_mesa.save
      format.html { redirect_to recepcions_url }
      format.json { head :no_content }
    end
  end

  def findCliente
    cliente = Cliente.find_by(Nu_Dni: params[:id])
    @nombre = cliente.No_Cliente
    @paterno = cliente.Ape_Paterno
    @materno = cliente.Ape_Mateno
    respond_to do |format|
        format.js
    end
  end

  private
    def recepcion_params
      params.require(:recepcion).permit(:cliente_id, :fe_recepcion, :hora_recepcion, :mesa_id, :tipo_recepcion, :empleado_id, :tx_detalle)
    end

    def set_recepcion
      @recepcion = Recepcion.find(params[:id])
      @upd_mesa = Mesa.find(@recepcion.mesa_id)
    end

  public
    #METODO PARA OBTENER EL NOMBRE DEL EQUIPO
    def getNameEmpleado(id)
      @equipo = Empleado.find(id)
      name = @equipo.No_Empleado
      return name
    end
  helper_method :getNameEmpleado
end
