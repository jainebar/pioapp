# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150807082340) do

  create_table "cargos", force: :cascade do |t|
    t.string   "No_Cargo"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "categoria", force: :cascade do |t|
    t.string   "Co_Categoria"
    t.string   "No_Categoria"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "clientes", force: :cascade do |t|
    t.string   "Co_Cliente"
    t.string   "Nu_Dni"
    t.string   "No_Cliente"
    t.string   "Ape_Paterno"
    t.string   "Ape_Mateno"
    t.date     "Fe_Ingreso"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "details", force: :cascade do |t|
    t.string   "Cod_Detalle"
    t.integer  "Qt_Cantidad"
    t.decimal  "Sa_PrecioUniSol"
    t.decimal  "Sa_ImporteSol"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  create_table "detalles", force: :cascade do |t|
    t.string   "orden_id"
    t.integer  "Qt_Cantidad"
    t.decimal  "Sa_PrecioUniSol"
    t.decimal  "Sa_ImporteSol"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "producto_id"
  end

  create_table "empleados", force: :cascade do |t|
    t.integer  "Co_Empleado"
    t.integer  "Nu_Dni"
    t.string   "No_Empleado"
    t.string   "Tx_Direccion"
    t.string   "Co_Departamento"
    t.string   "Co_Provincia"
    t.string   "Co_Distrito"
    t.string   "Tx_Email"
    t.date     "Fe_Nacimiento"
    t.string   "Tx_telefono"
    t.string   "Fl_Sexo"
    t.string   "Fl_EstadoCivil"
    t.date     "Fe_Contrato"
    t.date     "Fe_Culminacion"
    t.decimal  "Sa_Salario"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "cargo_id"
  end

  create_table "mesas", force: :cascade do |t|
    t.string   "Co_Mesa"
    t.integer  "Qt_CantidadSillas"
    t.integer  "Nu_Mesa"
    t.string   "Tipo_Mesa"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.string   "free"
  end

  create_table "orders", force: :cascade do |t|
    t.string   "Co_Pedido"
    t.decimal  "Sa_Subtotal"
    t.decimal  "Sa_Igv"
    t.decimal  "Sa_Neto"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "empleado_id"
    t.integer  "mesa_id"
  end

  create_table "productos", force: :cascade do |t|
    t.string   "Co_Producto"
    t.string   "No_Producto"
    t.decimal  "Qt_CostoProducto"
    t.integer  "Nu_CantidadProducto"
    t.date     "Fe_MovimientoProducto"
    t.date     "Fe_VencimientoProducto"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.integer  "categoria_id"
  end

  create_table "recepcions", force: :cascade do |t|
    t.integer  "cliente_id"
    t.date     "fe_recepcion"
    t.time     "hora_recepcion"
    t.integer  "mesa_id"
    t.string   "tipo_recepcion"
    t.integer  "empleado_id"
    t.text     "tx_detalle"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.integer  "level"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true

end
