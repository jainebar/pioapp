class CreateDetails < ActiveRecord::Migration
  def change
    create_table :details do |t|
      t.string :Cod_Detalle
      t.integer :Qt_Cantidad
      t.decimal :Sa_PrecioUniSol
      t.decimal :Sa_ImporteSol

      t.timestamps null: false
    end
  end
end
