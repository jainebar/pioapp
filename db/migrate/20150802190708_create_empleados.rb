class CreateEmpleados < ActiveRecord::Migration
  def change
    create_table :empleados do |t|
      t.integer :Co_Empleado
      t.integer :Nu_Dni
      t.string :No_Empleado
      t.string :Tx_Direccion
      t.string :Co_Departamento
      t.string :Co_Provincia
      t.string :Co_Distrito
      t.string :Tx_Email
      t.date :Fe_Nacimiento
      t.string :Tx_telefono
      t.string :Fl_Sexo
      t.string :Fl_EstadoCivil
      t.date :Fe_Contrato
      t.date :Fe_Culminacion
      t.decimal :Sa_Salario

      t.timestamps null: false
    end
  end
end
