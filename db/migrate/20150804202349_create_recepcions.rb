class CreateRecepcions < ActiveRecord::Migration
  def change
    create_table :recepcions do |t|
      t.integer :cliente_id
      t.date :fe_recepcion
      t.time :hora_recepcion
      t.integer :mesa_id
      t.string :tipo_recepcion
      t.integer :empleado_id
      t.text :tx_detalle

      t.timestamps null: false
    end
  end
end
