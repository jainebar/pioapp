Rails.application.routes.draw do
  resources :details
  resources :detalles

  #Routes:::Orders
  get '/orders/doit/:id', to: 'orders#doit', as: 'doit'
  get '/orders/listaproductos/:id', to: 'orders#listaproductos', as: 'listaproductos'
  get '/ajaxorden', to: 'orders#listaproductos', as: 'ajaxorden'
  post '/orders/saveDetails', to: 'orders#saveDetails', as: 'saveDetails'
  resources :orders

  resources :categoria
  resources :empleados
  devise_for :users
  
  #recepcions
  get '/recepcions/findCliente/:id', to: 'recepcions#findCliente', as: 'findCliente'
  resources :recepcions

  resources :cargos
  get "panel/index"
  #get "recepcion" => "recepcion#index"
  #get "recepcion/new"
  #post "recepcion" => "recepcion#create"
  #get "welcome/index"      

  root :to => "panel#index"
  #root to: 'panel#index'

  #concern del paginator
  concern :paginatable do
    get '(page/:page)', :action => :index, :on => :collection, :as => ''
  end

  #routes que aplican paginatable
  resources :mesas, :concerns => :paginatable
  resources :productos, :concerns => :paginatable
  #fin mesa

  resources :clientes
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
